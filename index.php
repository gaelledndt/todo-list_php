<?php
//J'appelle mon fichier de fonctions CRUD
require_once 'crud.php';
//Toutes mes dates ont pour le moment une date de création au jour même.
$date_crea = time();
$task_id;
$toupdate;
session_start();
if (isset($_POST['create'])) {
    create($_POST['title'], $_POST['description'], $date_crea);
}
if (isset($_POST['delete']) and is_numeric($_POST['delete'])) {
    deleteOne($_POST['delete']);
}
if (isset($_POST['update']) and is_numeric($_POST['update'])) {
    //Je lance une session
    session_start();
    //Pour éviter de passer de pages en pages et pour que tout se fasse sur la page principale, je stocke l'élément à modifier dans une session.
    $_SESSION['toupdate'] = readOne($_POST['update']);
}
//Je vérifie si une session To Update existe (si c'est le cas, c'est que j'ai au préalable cliqué sur update d'un item)
if ($_SESSION['toupdate']) {
    //Si j'ai validé le formulaire de modif de l'élément,
    if (isset($_POST['updateOne'])) {
        //Je relance la session
        session_start();
        //J'update l'élément avec les param obligatoires (au minimum)
        update($_SESSION['toupdate']['id'], $_POST['title'], $_POST['description'], $date_crea);
        //Je détruit les variables stockées dans ma session
        session_unset();
    }
}

//Je stocke toutes mes tâches dans une variable
$taskslist = readAll();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <title>To Do List</title>
</head>

<body>
    <div class="container">
        <h1>Ma liste de tâches</h1>
        <div>
            <h2> Ajouter une nouvelle tâche à ma liste</h2>
            <form action="" method="post">
                <div class="form-group">
                    <label class="form-label" for="title">Titre: </label>
                    <input class="form-control" type="text" name="title">
                </div>
                <div class="form-group">
                    <label class="form-label" for="description">Ma tâche: </label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
                <div class="form-group">
                    <label class="form-label" for="date_end">Date de fin: </label>
                    <input class="form-control" type="date" name="date_end">
                </div>
                <button class="btn btn-success" type="submit" name="create">Créer une nouvelle tâche</button>
            </form>
        </div>

        <!-- Si la session To Update existe, j'affiche un formulaire avec en valeur par défaut, les valeurs de ma tâche 
        à modifier que j'ai stocké plus tôt-->
        <?php if ($_SESSION['toupdate']) : ?>
            <div>
                <h2> Modifier ma tâche</h2>
                <form action="" method="post">
                    <div class="form-group">
                        <label class="form-label" for="title">Titre: </label>
                        <input class="form-control" type="text" name="title" value=<?php echo $_SESSION['toupdate']['title'] ?>>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="description">Ma tâche: </label>
                        <textarea class="form-control" name="description"><?php echo $_SESSION['toupdate']['description'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="date_end">Date de fin: </label>
                        <input class="form-control" type="date" name="date_end" <?php if (is_null($_SESSION['toupdate']['date_end'])) : ?> value="" <?php else : ?> value=<?php echo $_SESSION['toupdate']['date_end'] ?>>
                    <?php endif ?>
                    </div>
                    <button class="btn btn-success" type="submit" name="updateOne" value=<?php $_POST['toupdate']['id'] ?>>Modifier</button>
                </form>
            </div>
<!--Sinon  j'affiche dans des cards (pour un affichage sympa) mes taĉhes en cours-->
        <?php else : ?>
            <h2>Mes tâches en cours</h2>
            <?php foreach ($taskslist as $task) : ?>
                <div class="card">
                    <div class="card-body">
                        <h3 class="card_title"><?php echo ($task['title']) ?></h3>
                        <p class="card-text">
                            <?php echo ($task['description']) ?>
                        </p>
                    </div>
<!-- On retrouvera ici les deux boutons Modif et Suppression dont l'algo se trouve plus haut-->
                    <form action="" name="delete" method="post">
                        <button class="btn btn-danger" type="submit" name="delete" value=<?= $task['id'] ?>>Supprimer</button>
                    </form>
                    <form action="" name="update" method="post">
                        <button class="btn btn-primary" type="submit" name="update" value=<?= $task['id'] ?>>Modifier</button>
                    </form>
                </div>
            <?php endforeach ?>
        <?php endif ?>

    </div>
</body>

</html>