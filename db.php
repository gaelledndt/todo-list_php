<?php
//J'ai fait le choix de stocker mon algo de connexion dans un fichier à part et d'en faire un fonction pour
// pouvoir la réutiliser à foison. ( Oui j'ai utilisé le mot foison +1 point pour Griffondor)  
require_once 'globals.php';
//J'appelle le fichier globals.php que j'ai créée pour séparer mes id du reste du code (mais aussi pour pouvoir vous caster l'algo 
//à l'écran sans que vous ne voyez mes id de co à la db). Donc cette étape n'est pas obligatoire. 
//Si vous le faites, n'oubliez pas de créer le fichier globals et d'y insérer vos id.
//ATTENTION JE NE VOUS PARTAGE PAS MON FICHIER DANS LEQUEL SE TROUVE MES ID (pour des raisons evidentes)
// => N'OUBLIEZ PAS DE CREER CE MEME FICHIER OU D'INSERER VOS ID DANS DB.PHP!
//DU COUP, vu que j'ai mes id ailleurs, je les ai stocké dans la variable global $_GLOBALS (native au php)
//pour y avoir accès de tout mon projet.
function db_connect() {
    //La variable $_GLOBALS  est un tableau associatif
    $user = $GLOBALS['userdb'];
    $pass = $GLOBALS['passdb'];
    //On n'oublie pas d'englober notre tentative de connexion d'un try catch
    try {
        return $pdo = new PDO('mysql:host=localhost;dbname=todolist', $user, $pass);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
         return die();
    }
}