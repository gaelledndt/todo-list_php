<?php
//Je stocke dans ce fichier tout mon CRUD afin de faciliter le maintient du code et la lisibilitéprint_r
// Je n'oublie pas d'appeler le fichier dans lequel j'ai codé mon algo de connexion
require 'db.php';
//Je commence par le Read du CRUD (CreateReadUupdateDelete) et je le divise en 2 : 
//un qui me permettra de tout rechercher et un qui ne cherchera qu'un élément.
function readAll()
{
    //Je me connecte à  la db
    $pdo = db_connect();
    $query = "SELECT * FROM task";
    $request = $pdo->query($query);
    $result = $request->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function readOne($id)
{
    //Je me connecte à  la db
    $pdo = db_connect();
    $queryid = 'SELECT * FROM task where id = :id';
    $requestid = $pdo->prepare($queryid);
    //Comme j'utilise une requête préparée et que j'ai choisi la méthode de paramètre nommé, 
    //je n'oublie pas de bindParam mon paramètre 
    $requestid->bindParam(':id', $id);
    $requestid->execute();
    //Je fetch ma requête
    $result = $requestid->fetch();
    //Et retourne le résultat
    return($result);
}

//Pour le create, je passe en paramètres tous les éléments nécéssaires à la creation de ma nouvelle tâche
//Je set certains de ces paramètres par défault afin de me faciliter la vie mais c'est optionnel.
//D'ailleurs le fait d'affilier une valeur à un paramètre par défaut rend le paramètre optionnel au moment
//d'utiliser la fonction (on se donne rdv dans le fichier index.php ou create.php pour mieux saisir le bail) 
function create(string $title, string $description, int $date_crea, int $date_end = null, $is_checked = false)
{
    //Je me connecte à  la db
    $pdo = db_connect();
    $querycreate = "INSERT INTO task (title, description, date_crea, date_end, is_checked) VALUES (:title, :description, :date_crea, :date_end, :is_checked)";
    $requestcreate = $pdo->prepare($querycreate);
    //pour vous montrer les différentes manières de faire => ici je bind les paramètres 
    //directement dans le execute avec un tableau
    $requestcreate->execute(array(':title' => $title, ':description' => $description, ':date_crea' => $date_crea, ':date_end' => $date_end, ':is_checked' => $is_checked));
}

function deleteOne(int $id)
{
    //Je me connecte à  la db
    $pdo = db_connect();
    $querydelete = "DELETE FROM task WHERE id=$id";
    $requestdelete = $pdo->prepare($querydelete);
    $requestdelete->execute();
}

//J'ai besoin des mêmes infos que pour le create mais aussi de l'id de l'élément à modifier pour la version simplifiée du update
function update(int $id, string $title, string $description, int $date_crea, int $date_end = null, $is_checked = false)
{
    //Je me connecte à  la db
    $pdo = db_connect();
    $query = "UPDATE task set title = :title, description = :description, date_crea = :date_crea, date_end = :date_end, is_checked = :is_checked WHERE  id = :id";
    $request = $pdo->prepare($query);
    $request->execute(array(':title' => $title, ':description' => $description, ':date_crea' => $date_crea, ':date_end' => $date_end, ':is_checked' => $is_checked, 'id' => $id));
}
